import React from "react";
import styled from "styled-components";

const InputRadio = (
  { field, value, form, id, name, checked, onChange, label },
  ...props
) => (
  <Content>
    <InputContent
      id={id}
      checked={checked}
      type="radio"
      onChange={onChange}
      {...props}
    />
    <span />
    <Title>{label}</Title>
    <FormControl>
      {form.touched[field.name] && form.errors[field.name] && (
        <Error>{form.errors[field.name]}</Error>
      )}
    </FormControl>
  </Content>
);

InputRadio.defaultProps = {
  type: "text",
  placeholder: ""
};

export default InputRadio;

const Content = styled.label`
  margin-bottom: 5px;
  position: relative;
  display: flex;
  align-items: center;
`;

const FormControl = styled.div`
  position: relative;
`;

const Error = styled.span`
  color: rgba(250, 134, 134);
  position: absolute;
  left: 0;
  top: 100%;
  margin-top: 5px;
  font-size: 14px;
`;

const InputContent = styled.input`
  padding: 0.5rem;
  font-size: 16px;
  width: 100%;
  display: block;
  border-radius: 4px;
  border: 1px solid #ccc;
  border-color: ${({ error }) => (error ? "#007eff" : "#ccc")};
  &:focus {
    border-color: #007eff;
    box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075),
      0 0 0 3px rgba(0, 126, 255, 0.1);
    outline: none;
  }

  position: absolute;
  opacity: 0;
  & + span {
    position: relative;
    cursor: pointer;
    &:before {
      content: "";
      margin-right: 10px;
      display: block;
      width: 22px;
      height: 22px;
      background: #e1e1e1;
      border-radius: 50%;
      border: 1px #ccc solid;
    }
  }

  &:hover + span:before {
    background: #3ec168;
  }

  &:checked + span:before {
    background: #3ec168;
  }

  &:checked + span:after {
    content: "";
    position: absolute;
    left: 6px;
    top: 11px;
    background: white;
    width: 2px;
    height: 2px;

    box-shadow: 4px 0 0 white, 2px 0 0 white, 4px -2px 0 white, 4px -6px 0 white,
      4px -8px 0 white, 4px -4px 0 white;
    transform: rotate(45deg);
  }
`;

const Title = styled.div``;
