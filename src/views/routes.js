import React, { useEffect } from 'react';
import styled from 'styled-components';
import { Switch, Route } from 'react-router-dom';
import { Container, media } from 'styles';
import { Helmet } from 'react-helmet';

import Header from 'components/Header';
import Footer from 'components/Footer';
import Sidebar from 'components/Sidebar';
import { LoadingFixed } from 'components/UI/Loading';
import Modal from 'components/Modals';

import { getHash, setHash } from 'lib/helpers/token';
// redux
import { connect } from 'react-redux';
import { selectors, actions } from 'redux/ducks/main.duck';
import {
  selectors as selectorsUser,
  actions as actionsUser,
} from 'redux/ducks/user.duck';
import {
  selectors as selectoresPromotions,
  actions as actionsPromotions,
} from 'redux/ducks/promotions.duck';
import { actions as actionsModals } from 'redux/ducks/modals.duck';
import { history } from '../redux/store';
// routes
import Home from './Home';
import Categories from './Categories';
import Detail from './Detail';

function Routes(props) {
  const { loading, settings } = props;

  function handleSeo() {
    // eslint-disable-next-line prefer-destructuring
    const title = document.title;

    if (title === settings.appTitle.value) {
      return false;
    }
    return true;
  }

  useEffect(() => {
    const {
      fetchCategoriesRequest,
      fetchSettingsRequest,
      fetchPopularsRequest,
      fetchTagsRequest,
      fetchSocialsRequest,
      fetchPromotionsRequest,
      fetchProfileRequest,
      openModal,
    } = props;

    const searchParams = new URLSearchParams(history.location.search);

    const gettingHash = () => {
      if (searchParams.get('hash')) {
        setHash(searchParams.get('hash'));
        history.replace('', null);
      }

      return getHash();
    };

    if (searchParams.get('reset_token') && searchParams.get('email')) {
      openModal({
        type: 'resetPassword',
        args: [...searchParams.entries()].reduce((acc, current) => {
          return { ...acc, [current[0]]: current[1] };
        }, {}),
      });
    }

    if (gettingHash()) {
      fetchProfileRequest(gettingHash());
    }

    fetchPromotionsRequest();
    fetchCategoriesRequest();
    fetchSettingsRequest();
    fetchPopularsRequest();
    fetchTagsRequest();
    fetchSocialsRequest();
  }, []);

  return loading ? (
    <LoadingFixed />
  ) : (
    <Content id="wrapper">
      {settings.appTitle && (
        <div>
          {handleSeo() && (
            <Helmet>
              <title>{settings.appTitle.value}</title>
              <meta
                name="description"
                content={settings.appDescription.value}
              />
              <meta name="keywords" content={settings.appKeywords.value} />
            </Helmet>
          )}
        </div>
      )}
      <Header {...props} />
      <MainContent>
        <Switch>
          <Route
            exact
            path="/"
            render={() => (
              <MainContainer>
                <Home settings={settings} />
                <Sidebar {...props} />
              </MainContainer>
            )}
          />
          <Route
            exact
            path="/search"
            render={rest => (
              <MainContainer>
                <Categories {...rest} />
                <Sidebar {...props} />
              </MainContainer>
            )}
          />
          <Route path="/article/:id" component={Detail} />
          <Route render={() => <h2>Такой нет страницы</h2>} />
        </Switch>
      </MainContent>
      <Footer {...props} />
      <Modal />
    </Content>
  );
}

export default connect(
  state => ({
    categories: selectors.getCategories(state),
    populars: selectors.getPopulars(state),
    loading: selectors.getLoading(state),
    tags: selectors.getTags(state),
    socials: selectors.getSocials(state),
    settings: selectors.getSettings(state),
    promotions: selectoresPromotions.getPromotions(state),
    loadingPromotions: selectoresPromotions.getLoading(state),
    profile: selectorsUser.getProfile(state),
  }),
  { ...actions, ...actionsPromotions, ...actionsUser, ...actionsModals },
)(Routes);

const Content = styled.div``;

const MainContent = styled.div`
  ${Container}
  padding-top: 40px;
  margin-bottom: 60px;
  width: 100%;
`;

const MainContainer = styled.div`
  display: grid;
  width: 100%;
  margin: 0 auto;
  grid-template-columns: 780px auto;
  grid-gap: 0 30px;
  ${media.desktop`
    grid-template-columns: 100%;
  `}
`;
