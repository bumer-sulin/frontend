import React from "react";
import { Route, Redirect } from "react-router-dom";

const PrivateRoute = ({ component: Component, isAuth, ...rest }) => (
  <Route
    {...rest}
    render={props => {
      return isAuth ? (
        <Component {...props} />
      ) : (
        <Redirect
          to={{ pathname: 'signin', state: { from: props.location } }}
        />
      );
    }}
  />
);

export default PrivateRoute;
