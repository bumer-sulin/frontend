export default (search, category_id, tags, date) => {
  const params = new URLSearchParams('');

  if (search) {
    params.append('search', search);
  }

  if (category_id) {
    params.append('category_id', category_id);
  }

  if (tags) {
    tags.forEach(e => {
      params.append('tags[]', e);
    });
  }

  if (date) {
    params.append('date', date);
  }

  return params.toString();
};
