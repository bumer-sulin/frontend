import { createStore, applyMiddleware } from 'redux';
// import { createLogger } from 'redux-logger';
import { createBrowserHistory } from 'history';
import { routerMiddleware } from 'connected-react-router';
import thunk from 'redux-thunk';
import createSagaMiddleware from 'redux-saga';
import sagasManager from 'lib/sagasManager';
import rootReducer from './rootReducer';

const sagaMiddleware = createSagaMiddleware();
const initialState = {};

export const history = createBrowserHistory();
export const store = createStore(
  rootReducer(history),
  initialState,
  applyMiddleware(
    // createLogger(),
    sagaMiddleware,
    thunk,
    routerMiddleware(history),
  ),
);

export default () => {
  sagaMiddleware.run(sagasManager.getRootSaga());

  return { store, history };
};
