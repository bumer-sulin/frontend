import { createPortal } from 'react-dom';

const MainPortal = ({ children, modalRoot }) =>
  createPortal(children, modalRoot);

MainPortal.defaultProps = {
  modalRoot: document.getElementById('modal'),
};

export default MainPortal;
