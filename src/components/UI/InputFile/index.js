import React from "react";
import styled from "styled-components";
import Thumb from "./Thumb";

const InputFile = ({ values, name, form, title, image, ...props }) => (
  <Content>
    {image && <Thumb file={values[name]} />}
    <Item>
      {title && <Label>{title}</Label>}
      <input
        type="file"
        name={name}
        accept={image ? "image/*" : ""}
        // {...props}
        onChange={event => {
          props.setValues({ ...values, [name]: event.target.files[0] });
        }}
      />
    </Item>
  </Content>
);

export default InputFile;

const Item = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  grid-gap: 16px 0;
  margin-top: 40px;
  position: relative;
`;

const Content = styled.div`
  max-width: 400px;
  width: 100%;
`;

const Label = styled.label``;
