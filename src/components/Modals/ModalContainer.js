import React from 'react';
import styled from 'styled-components';
import { media } from 'styles';

const ModalContainer = ({
  closeModal,
  openModal,
  maxWidth,
  title,
  children,
  marginTop,
  center,
}) => (
  <React.Fragment>
    <CloseBackground onClick={() => closeModal()} />
    <ModalDialog maxWidth={maxWidth} marginTop={marginTop} center={center}>
      <div>
        <Header onClick={() => closeModal()}>
          <ButtonContainer>
            <button type="button">
              <span aria-hidden="true">&times;</span>
            </button>
          </ButtonContainer>
        </Header>
        {React.cloneElement(children, { closeModal, openModal })}
      </div>
    </ModalDialog>
  </React.Fragment>
);

export default ModalContainer;

// ModalContainer.defaultProps = {
//   // maxWidth: '100%',
//   // title: '',
// };

const ModalDialog = styled.div`
  /* padding: 20px 20px 40px; */
  margin: 0 auto;
  margin-top: ${({ marginTop }) => (marginTop ? `${marginTop}px` : '0px')};
  width: ${({ maxWidth }) => (maxWidth ? '100%' : 'initial')};
  max-width: ${({ maxWidth }) => maxWidth || 'initial'};
  position: relative;
  /* padding: 2rem 0; */
  /* display: flex;
  flex-direction: column;
  align-items: center; */
  justify-content: ${({ center }) => (center ? 'center' : 'start')};
  ${media.phone`
    margin: 40px 0;
    /* padding: 40px 20px; */
  `}
`;

const CloseBackground = styled.div`
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  position: fixed;
  width: 100%;
  height: 100%;
  -webkit-overflow-scrolling: touch;
  z-index: -1;
`;

const ButtonContainer = styled.div`
  /* min-width: 30px;
  min-height: 30px; */
  cursor: pointer;
  display: flex;
  align-items: center;
  justify-content: center;
  button {
    font-size: 46px;
    color: #fff;
    background: none;
    border: none;
    pointer-events: none;
    cursor: pointer;
  }
`;

const Header = styled.div`
  display: grid;
  justify-content: end;
  width: 100%;
  position: relative;
`;

const Title = styled.h4``;
