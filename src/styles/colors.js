const colors = {
  whiteSmoke: '#fbfbfd',
  grey: '#a7b3c6',
  green: '#4BB92F',
  orange: '#ff8700',
  violet: '#8d00ff',
  blue: '#0078ff',
  black: '#212631',
  red: ' #f13d37',
};

export { colors };
