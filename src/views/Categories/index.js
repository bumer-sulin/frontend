import React, { useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { connect } from 'react-redux';
import { LoadingDefault } from 'components/UI/Loading';
import { selectors, actions } from 'redux/ducks/category.duck';
import styled from 'styled-components';
import Filter from './Filter';
import List from './List';

function Categories({
  location,
  loading,
  items,
  fetchDetailsCategoryRequest,
  history,
  pagination,
}) {
  const scroll = useRef(null);
  const scrollItems = useRef(null);
  const [searchParams, setSearchParams] = useState(
    new URLSearchParams(location.search),
  );

  useEffect(() => {
    const params = new URLSearchParams(location.search);
    if (
      [...params.keys()].find(e => e === 'category_id') ||
      [...params.keys()].find(e => e === 'search') ||
      [...params.keys()].find(e => e === 'tags[]')
    ) {
      scroll.current.scrollIntoView({
        behavior: 'smooth',
        block: 'start',
        inline: 'nearest',
      });
    }
  }, [location.search]);

  useEffect(() => {
    setSearchParams(new URLSearchParams(location.search));
    fetchDetailsCategoryRequest(location.search);
  }, [location.search]);

  // useEffect(() => {
  //   fetchDetailsCategoryRequest(location.search);
  // }, []);

  function handleScroll() {
    scrollItems.current.scrollIntoView({
      behavior: 'smooth',
      block: 'start',
      inline: 'nearest',
    });
  }

  return (
    <Content ref={scroll}>
      <Helmet>
        <title>{`Поиск по категориям ${
          searchParams.get('search')
            ? searchParams.get('search')
            : 'Все Новости'
        }`}</title>
        <meta name="description" content="Nested component" />
      </Helmet>
      <div>
        <Filter
          searchText={searchParams.get('search')}
          categoryId={searchParams.get('category_id')}
          tagsIds={searchParams.getAll('tags[]')}
          urlDate={searchParams.get('date')}
          history={history}
          handleScroll={handleScroll}
        />
        <div ref={scrollItems}>
          {loading ? (
            <LoadingDefault />
          ) : (
            <List
              items={items}
              categoryId={searchParams.get('category_id')}
              location={location}
              history={history}
              pagination={pagination}
              fetchDetailsCategoryRequest={fetchDetailsCategoryRequest}
              handleScroll={handleScroll}
            />
          )}
        </div>
      </div>
    </Content>
  );
}

Categories.propTypes = {
  items: PropTypes.instanceOf(Array).isRequired,
  fetchDetailsCategoryRequest: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired,
  history: PropTypes.shape({}).isRequired,
  location: PropTypes.shape({}).isRequired,
};

export default connect(
  state => ({
    loading: selectors.getLoading(state),
    items: selectors.getItems(state),
    pagination: selectors.getPagination(state),
  }),
  { ...actions },
)(Categories);

const Content = styled.div``;
