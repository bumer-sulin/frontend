import axios from 'axios';
// import { getToken } from './token';

// const API_URL = `${process.env.REACT_APP_API_URL}`;

const API_URL = `${document
  .querySelector('meta[name=api]')
  .getAttribute('content')}/api`;

export default async (method, url, options = {}, config = null) => {
  const instance = axios.create({
    baseURL: API_URL,
    headers: {
      Authorization: 'Basic YnVtZXI6RlNNUnlUTXFRYjdhekFRTA==',
      // Authorization: `Bearer ${getToken()}`,
    },
  });
  try {
    const response = await instance[method](url, options, config);

    return response.data;
  } catch (err) {
    const error = await err.response;

    throw error;
  }
};
