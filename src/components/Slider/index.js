import React, { useRef } from 'react';
import { connect } from 'react-redux';
import { actions } from 'redux/ducks/modals.duck';
import styled from 'styled-components';
import Swiper from 'react-id-swiper';
import AlternativeArticle from 'components/AlternativeArticle';
import { MdArrowBack, MdArrowForward } from 'react-icons/md';
import { ResetDefaultButton, media } from 'styles';

function Slider({ items, gallery, openModal }) {
  const swiper = useRef(null);

  function onMouseEnter() {
    swiper.current.swiper.autoplay.pause();
  }

  function onMouseLeave() {
    swiper.current.swiper.autoplay.run();
  }

  function handleClick(index, galleryImages) {
    openModal({
      type: 'gallery',
      args: {
        galleryImages,
        index,
      },
    });
  }

  const params = {
    // centeredSlides: true,
    // loop: true,
    // draggable: false,
    noSwiping: true,
    autoplay: {
      delay: 5000,
      disableOnInteraction: true,
    },
    renderPrevButton: () => (
      <Button className="button-prev">
        <MdArrowBack size={25} />
      </Button>
    ),
    renderNextButton: () => (
      <Button className="button-next">
        <MdArrowForward size={25} />
      </Button>
    ),
    navigation: {
      nextEl: '.button-next',
      prevEl: '.button-prev',
    },
    breakpoints: {
      768: {
        autoplay: false,
      },
    },
    observer: true,
  };

  return (
    items.length > 0 && (
      <ContainerCarousel
        onMouseEnter={onMouseEnter}
        onMouseLeave={onMouseLeave}
      >
        <Swiper ref={swiper} {...params}>
          {items.map((slide, index) => (
            <div key={slide.id}>
              {gallery && (
                <OpenGallery onClick={() => handleClick(index, items)} />
              )}
              <AlternativeArticle {...slide} index={index} />
            </div>
          ))}
        </Swiper>
      </ContainerCarousel>
    )
  );
}

export default connect(
  null,
  { ...actions },
)(Slider);

const ContainerCarousel = styled.div`
  max-width: 100%;
  width: 100%;
  position: relative;
  margin-bottom: 40px;
  overflow: hidden;
  .button-prev,
  .button-next {
    position: absolute;
    bottom: 0;
    right: 0;
    z-index: 100;
    padding: 20px;
    color: #fff;
    transition: all 0.2s ease;
    &:hover {
      background: rgba(255, 255, 255, 0.1);
    }

    ${media.phone`
      top: 20px;
      bottom: inherit;

      &:first-child {
        padding: 20px 40px 20px 5px;
      }
      &:last-child {
        padding: 20px 5px 20px 40px;
      }
      &:hover {
        background: none;
      }
  `}
  }

  .button-prev {
    right: 65px;
    ${media.phone`
      right: inherit;
      left: 0;
    `}
  }
  .button-next {
    ${media.phone`
      right: 0;
    `}
  }
`;

const Button = styled.button`
  ${ResetDefaultButton}
  padding: 20px;
  color: #fff;
  transition: all 0.2s ease;
  &:hover {
    background: rgba(255, 255, 255, 0.1);
  }

  ${media.phone`
    &:first-child {
      padding: 20px 40px 20px 5px;
    }
    &:last-child {
      padding: 20px 5px 20px 40px;
    }
    &:hover {
      background: none;
    }
  `}
`;

const OpenGallery = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  height: 100%;
  width: 100%;
  z-index: 999;
  cursor: pointer;
  transition: all 0.3s ease;
  &:hover {
    opacity: 0.8;
  }
`;
