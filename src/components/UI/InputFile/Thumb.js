import React from "react";
import styled from "styled-components";

class Thumb extends React.Component {
  state = {
    loading: false,
    thumb: null,
  };

  componentDidUpdate(prevProps, props) {
    if (prevProps.file !== this.props.file && this.props.file) {
      this.setState({ loading: true }, () => {
        let reader = new FileReader();

        reader.onloadend = () => {
          this.setState({ loading: false, thumb: reader.result });
        };

        reader.readAsDataURL(this.props.file);
      });
    }
  }

  render() {
    const { file } = this.props;
    const { loading, thumb } = this.state;

    if (!file) {
      return null;
    }

    if (loading) {
      return <p>loading...</p>;
    }

    return <Image src={thumb} alt={file.name} />;
  }
}

export default Thumb;

const Image = styled.div`
  width: 200px;
  height: 200px;
  background-size: contain;
  background-repeat: no-repeat;
  background-position: center;
  background-image: ${({ src }) => src && `url(${src})`};

  padding: 0.25rem;
  background-color: #fff;
  border: 1px solid #dee2e6;
  border-radius: 0.25rem;
`;
