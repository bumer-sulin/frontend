import React from 'react';
import { ThemeProvider } from 'styled-components';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';
import { colors, GlobalStyle } from 'styles';
import configureStore from 'redux/store';
import Routes from 'views/routes';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const { store, history } = configureStore();

const App = () => (
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <ThemeProvider theme={{ ...colors }}>
        <React.Fragment>
          <Routes />
          <GlobalStyle />
          <ToastContainer autoClose={3000} toastClassName="toast-custom" />
        </React.Fragment>
      </ThemeProvider>
    </ConnectedRouter>
  </Provider>
);

export default App;
