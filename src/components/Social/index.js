import React from 'react';
import styled from 'styled-components';
import {
  FaVk,
  FaOdnoklassniki,
  FaTwitter,
  FaTelegram,
  FaGoogle,
  FaFacebookF,
  FaWhatsapp,
  FaSkype,
  FaYoutube,
  FaYandex,
  FaInstagram,
} from 'react-icons/fa';

function getColor(type) {
  switch (type) {
    case 'vk':
      return '#4c75a3';
    case 'odnoklassniki':
      return '#f57d00';
    case 'twitter':
      return '#55acee';
    case 'telegram':
      return '#0077b5';
    case 'google':
      return '#4285f4';
    case 'facebook':
      return '#3b5999';
    case 'whatsapp':
      return '#25D366';
    case 'skype':
      return '#00AFF0';
    case 'youtube':
      return '#cd201f';
    case 'yandex':
      return '#e61400';
    case 'instagram':
      return '#c32aa3';
    default:
      return 'rgba(255,255,255,0.6);';
  }
}

function getSocial(type) {
  switch (type) {
    case 'vk':
      return <FaVk size={22} />;
    case 'odnoklassniki':
      return <FaOdnoklassniki size={22} />;
    case 'twitter':
      return <FaTwitter size={22} />;
    case 'telegram':
      return <FaTelegram size={22} />;
    case 'google':
      return <FaGoogle size={22} />;
    case 'facebook':
      return <FaFacebookF size={22} />;
    case 'whatsapp':
      return <FaWhatsapp size={22} />;
    case 'skype':
      return <FaSkype size={22} />;
    case 'youtube':
      return <FaYoutube size={22} />;
    case 'instagram':
      return <FaInstagram size={22} />;
    case 'yandex':
      return <FaYandex size={22} />;
    default:
      return <FaVk size={22} />;
  }
}

function Social({ type, link, title }) {
  return (
    <StyledLink
      href={link}
      title={title}
      target="_blank"
      color={getColor(type.title)}
    >
      {getSocial(type.title)}
    </StyledLink>
  );
}

export default Social;

const StyledLink = styled.a`
  width: 45px;
  height: 45px;
  border-radius: 50%;
  color: #fff;
  display: flex;
  align-items: center;
  justify-content: center;
  background: ${({ color }) => color};
`;
