const setHash = hash => {
  localStorage.setItem('hash', hash);

  return localStorage.getItem('hash');
};

const removeHash = () => localStorage.removeItem('hash');

const getHash = () => localStorage.getItem('hash');

export { setHash, removeHash, getHash };
