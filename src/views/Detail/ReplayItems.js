import React, { useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { MdThumbDown, MdThumbUp, MdReply } from 'react-icons/md';
import { ResetDefaultButton, media } from 'styles';

function ReplayItems({
  handleLike,
  handleDislike,
  handleComment,
  toggleComment,
  hasPermission,
  likes,
  dislikes,
  love,
  id,
}) {
  const [isLikes, setIsLikes] = useState({
    isActive: love || false,
    newLikes: likes,
    newDislikes: dislikes,
  });

  function setLike() {
    handleLike(id);
    setIsLikes(prevState => ({
      ...prevState,
      isActive: 'like',
      newLikes: prevState.newLikes + 1,
    }));
  }

  function setDislike() {
    handleDislike(id);

    setIsLikes(prevState => ({
      ...prevState,
      isActive: 'dislike',
      newDislikes: prevState.newDislikes + -1,
    }));
  }

  function toggleLikes(action) {
    if (action === 'like') {
      return hasPermission() && setLike();
    }

    return hasPermission() && setDislike();
  }

  return (
    <ReplayBtnGroup>
      <Likes isActive={isLikes.isActive}>
        <button
          type="button"
          onClick={() => toggleLikes('like')}
          disabled={isLikes.isActive}
        >
          <span>{isLikes.isActive ? isLikes.newLikes : likes}</span>
          <div>
            <MdThumbUp size={20} />
          </div>
        </button>
        <button
          type="button"
          onClick={() => toggleLikes('dislike')}
          disabled={isLikes.isActive}
        >
          <span>{isLikes.isActive ? isLikes.newDislikes : dislikes}</span>
          <div>
            <MdThumbDown size={20} />
          </div>
        </button>
      </Likes>
      <ReplayBtn onClick={() => handleComment(id)}>
        <MdReply size={24} /> {toggleComment === id ? 'Закрыть' : 'Ответить'}
      </ReplayBtn>
    </ReplayBtnGroup>
  );
}

export default ReplayItems;

ReplayItems.propTypes = {
  handleLike: PropTypes.func.isRequired,
  handleDislike: PropTypes.func.isRequired,
  handleComment: PropTypes.func.isRequired,
  toggleComment: PropTypes.string.isRequired,
  likes: PropTypes.number.isRequired,
  dislikes: PropTypes.number.isRequired,
  love: PropTypes.oneOfType([PropTypes.string, PropTypes.any]).isRequired,
  id: PropTypes.number.isRequired,
};

const ReplayBtnGroup = styled.div`
  display: grid;
  justify-content: end;
  margin-top: 30px;
  align-items: center;
  grid-auto-flow: column;
  grid-gap: 0 24px;

  ${media.phone`
    margin-top: 15px;
  `}
`;

const ReplayBtn = styled.button`
  ${ResetDefaultButton};
  color: #2775b1;
  display: flex;
  align-items: center;
  font-size: 16px;
  font-weight: 500;
  padding: 4px 8px 4px 4px;
  border-radius: 2px;
  transition: all 0.1s ease;
  svg {
    margin-right: 5px;
  }

  &:hover {
    background: rgba(39, 117, 177, 0.2);
  }
`;

const Likes = styled.div`
  display: grid;
  grid-auto-flow: column;
  grid-gap: 0 24px;
  button {
    ${ResetDefaultButton};
    display: inline-flex;
    align-items: center;
    transition: all 0.1s ease;

    &:disabled {
      cursor: default;
      &:first-child {
        & > div {
          &:hover {
            background: ${({ isActive }) =>
              isActive === 'like' ? 'rgba(54, 178, 116, 0.2)' : 'transparent'};
          }
        }
      }

      &:last-child {
        &:hover {
          & > div {
            background: ${({ isActive }) =>
              isActive === 'dislike'
                ? 'rgba(241, 61, 55, 0.2)'
                : 'transparent'};
          }
        }
      }
    }

    span {
      font-size: 14px;
      margin-right: 5px;
    }

    & > div {
      width: 34px;
      height: 34px;
      border-radius: 50%;
      display: inline-flex;
      align-items: center;
      justify-content: center;
      transition: all 0.1s ease;
    }

    &:first-child {
      color: #36b274;
      & > div {
        background: ${({ isActive }) =>
          isActive === 'like' ? 'rgba(54, 178, 116, 0.2)' : 'transparent'};
        &:hover {
          background: rgba(54, 178, 116, 0.2);
          opacity: 1;
        }
      }
    }

    &:last-child {
      color: ${({ theme }) => theme.red};
      & > div {
        background: ${({ isActive }) =>
          isActive === 'dislike' ? 'rgba(241, 61, 55, 0.2)' : 'transparent'};
        &:hover {
          background: rgba(241, 61, 55, 0.2);
          opacity: 1;
        }
      }
    }
  }
`;
