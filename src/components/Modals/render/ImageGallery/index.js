import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { MdArrowBack, MdArrowForward } from 'react-icons/md';
import { ResetDefaultButton } from 'styles';

function Quiz({ galleryImages, index }) {
  const [current, setCurrent] = useState(index);

  function prev() {
    if (current - 1 > 0) {
      return setCurrent(current - 1);
    }

    return setCurrent(0);
  }

  function next() {
    if (current + 1 < galleryImages.length) {
      return setCurrent(current + 1);
    }

    return setCurrent(galleryImages.length - 1);
  }

  return (
    <Content>
      <ImageContainer onClick={next}>
        <img src={galleryImages[current].image} alt="" />
      </ImageContainer>
      {current !== 0 && (
        <Button onClick={prev} className="left-button-image">
          <MdArrowBack size={40} />
        </Button>
      )}
      {current !== galleryImages.length - 1 && (
        <Button onClick={next} className="right-button-image">
          <MdArrowForward size={40} />
        </Button>
      )}
      <Footer>
        <span>Фото: {galleryImages[current].category.title}</span>
        <span>
          {current + 1} из {galleryImages.length}
        </span>
      </Footer>
    </Content>
  );
}

export default Quiz;

const Content = styled.div`
  position: relative;
  overflow: hidden;
  margin: 0 auto;
  display: inline-grid;
  justify-content: center;
  /* margin: 0 -15px; */
  img {
    display: block;
    height: auto;
    margin: 0 auto;
    max-width: 100%;

    user-select: none;
    cursor: pointer;
    max-height: calc(100vh - 210px);
  }
`;

const ImageContainer = styled.div`
  cursor: pointer;
`;

const Button = styled.button`
  ${ResetDefaultButton};
  position: fixed;
  top: 50%;
  transform: translateY(-50%);
  color: #fff;
  z-index: 100;
  width: 15%;
  height: 100%;
  display: flex;
  align-items: center;
  padding: 0 20px;
  transition: all 0.2s ease;
  &:hover {
    background: rgba(0, 0, 0, 0.1);
  }

  &.left-button-image {
    left: 0px;
    justify-content: flex-start;
  }

  &.right-button-image {
    right: 0px;
    justify-content: flex-end;
  }
`;

const Footer = styled.div`
  display: flex;
  margin-top: 15px;
  font-size: 14px;
  align-items: center;
  justify-content: space-between;
  color: #fff;
`;
